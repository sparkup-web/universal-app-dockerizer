#!/bin/sh

##########################################################
## Project Installation
##########################################################

WEB_DIR="web"

getFileExtension() {

    extesion=$(echo "$1" | awk -F'[.]' '{print $(NF-1)"."$NF}')

    case $extesion in
        *sql.gz*)
            echo "sql.gz"
            ;;
        *tar.gz|gz*)
            echo "tar.gz"
            ;;
        *bz2*)
            echo "tar.bz2"
            ;;
        *zip*)
            echo "zip"
            ;;
        *)
            echo $extesion
    esac

}

if [ ! "$(ls -A ${WEB_DIR})" ]; then
    
    echo "Processing source code"

    IFS=" "
    set ${APP_SOURCES}
    for source
    do
        case $source in
            *framework*)
                if [ -f "source/${source}" ]; then
                    extension=${source##*.}
                    tar_options="xvf"

                    if [ $extension == "gz" ]; then
                        tar_options="xvzf"
                    elif [ $extension == "bz2" ]; then
                        tar_options="xvjf"
                    fi

                    tar -$tar_options source/${source} -C ${WEB_DIR}
                    dir=$(find ${WEB_DIR} -mindepth 1 -maxdepth 1 -type d)
                    mv $dir/* $dir/.* ${WEB_DIR}
                    rm -rf $dir
                fi
                ;;
            *)
                echo "no match found in ${source}"
        esac

    done
    
fi

# Initalize Wordpress
if [ ! -f "web/wp-config.php" ]; then
    
    echo "Creating Wordpress config file"

    # Generate config file
    wp config create --allow-root --path="${WEB_DIR}" --dbhost="${DATABASE_HOST}" --dbname="${DATABASE_NAME}" --dbuser="${DATABASE_USER}" --dbpass="${DATABASE_PASSWORD}" --locale=AU
    
fi

if [ -f "web/wp-config.php" ]; then

    # Install WordPress
    wp core is-installed --allow-root --path="${WEB_DIR}"

    if [ $? -eq 1 ]; then
        echo "Installing Wordpress"
        sleep 20
        wp core install --allow-root --path="${WEB_DIR}" --url="${APP_HOST}" --title="${APP_NAME}" --admin_user="${ADMIN_USERNAME}" --admin_password="${ADMIN_PASSWORD}" --admin_email="${ADMIN_EMAIL}"
    fi

fi
