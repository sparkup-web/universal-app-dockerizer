#!/bin/sh

##########################################################
## Plugin Installation
##########################################################

WEB_DIR="web"

getFileExtension() {

    extesion=$(echo "$1" | awk -F'[.]' '{print $(NF-1)"."$NF}')

    case $extesion in
        *sql.gz*)
            echo "sql.gz"
            ;;
        *tar.gz|gz*)
            echo "tar.gz"
            ;;
        *bz2*)
            echo "tar.bz2"
            ;;
        *zip*)
            echo "zip"
            ;;
        *)
            echo $extesion
    esac

}

if [ "$(ls -A ${WEB_DIR})" ]; then
    
    echo "Installing plugins"

    IFS=" "
    set ${APP_PLUGINS}
    for plugin
    do
        case $plugin in
            *woocommerce*)
                if [ -f "source/${plugin}" ]; then

                    extension=$(getFileExtension $plugin)
                    version=$(echo "$plugin" | sed "s/^.*-\([0-9.]*\).$extension/\1/g")

                    wp plugin is-installed woocommerce-${version} --allow-root --path="${WEB_DIR}" 

                    if [ $? -eq 1 ]; then
                    
                        echo "Installing Woocommerce"
                        # wp plugin install --allow-root --path="${WEB_DIR}" woocommerce --version="3.5.1"
                        wp plugin install ./source/${plugin} --allow-root --path="${WEB_DIR}" 
                    fi

                    wp plugin is-active woocommerce-${version} --allow-root --path="${WEB_DIR}"

                    if [ $? -eq 1 ]; then
                        echo "Activating Woocommerce"
                        wp plugin activate woocommerce-${version} --allow-root --path="${WEB_DIR}"

                    fi

                else
                    version=$(echo "$plugin" | sed "s/^.*-\([0-9.]*\)/\1/g")
                    wp plugin is-installed ${plugin} --allow-root --path="${WEB_DIR}" 

                    if [ $? -eq 1 ]; then
                    
                        echo "Installing Woocommerce"
                        wp plugin install woocommerce --allow-root --path="${WEB_DIR}" --version="${version}"
                    fi

                    wp plugin is-active ${plugin} --allow-root --path="${WEB_DIR}"

                    if [ $? -eq 1 ]; then
                        echo "Activating Woocommerce"
                        wp plugin activate ${plugin} --allow-root --path="${WEB_DIR}"

                    fi

                   
                fi
                ;;
            *)
                echo "no match found in ${plugin} Plugin"
        esac

    done
    
fi

# update configurations
echo "Updating plugin configurations"

IFS="|"
set ${PLUGIN_CONFIGURATIONS}
for custom_config
do
    config_key=$(echo "$custom_config" | sed -e "s/=.*\$//")
    config_value=$(echo "$custom_config" | sed -e "s/\b[a-z_\/]*=//1" -e "s/\[\[:space:\]\]/ /g")

    if [ ! -z "$config_key" -a ! -z "$config_value" ]; then
        wp option update $config_key "$config_value" --allow-root --path="${WEB_DIR}"
    fi
    
done

