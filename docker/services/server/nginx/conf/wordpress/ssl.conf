server {
    listen 443 ssl http2;
    index index.php index.html;

    ssl_certificate /etc/ssl/certs/cacert.pem;        # path to your cacert.pem
    ssl_certificate_key /etc/ssl/certs/key.pem;    # path to your privkey.pem
    
    server_name localhost;
    error_log  /var/log/nginx/error.log;
    access_log /var/log/nginx/access.log;
    root /var/www/html;
	 
	# deny htaccess files 
	location ~ /\. { 
		deny  all; 
		access_log off; 
		log_not_found off; 
	} 
	 
	location ~*  \.(jpg|jpeg|png|gif|ico)$ { 
		expires 365d; 
		log_not_found off; 
		access_log off; 
	} 

	location = /favicon.ico {
		log_not_found off;
		access_log off;
	}

    location = /robots.txt {
		allow all;
		log_not_found off;
		access_log off;
	}

	location ~* \.(js|css|png|jpg|jpeg|gif|ico)$ {
		expires max;
		log_not_found off;
	}

	## rewrite anything else to index.php
	location / { 
		index index.html index.php;
		try_files $uri $uri/ /index.php?$query_string;
		expires 30d;
	}

    # pass the PHP scripts to FPM socket 
	location ~ \.php$ {
        try_files $uri =404;
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass platform:9000;
        fastcgi_index index.php;
		include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        fastcgi_param PATH_INFO $fastcgi_path_info;
    }

}