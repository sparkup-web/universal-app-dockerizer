# Universal App Dockerizer

This tool supports to build web applciations via docker

## Getting Started

These instructions will get you to set up local site on your local machine for development and testing purposes

### Prerequisites

What things you need to install the software and how to install them

* [Docker](https://www.docker.com/get-started) - The Dev to Ops Choice for Container Platforms
* [NodeJs](https://nodejs.org/en/) - The JavaScript runtime built on Chrome's V8 JavaScript engine.
* [Git](https://git-scm.com/) - The git tool to handle project version control and bash script

### Windows Users

**If your system doesn't recognize the bash script**, please go to this page to update your $PATH variables
https://helpdeskgeek.com/how-to/fix-not-recognized-as-an-internal-or-external-command/

### Running the local sites

A step by step series of examples that tell you how to get a local running

* Run the application via command line from root folder.

```
npm run <framework name>
```

Here is an example to run magento:

```
npm run magento
```

## Authors

* **Henry Fan** - *Initial work* - [henry.fan@sparkupweb.com](henry.fan@sparkupweb.com)